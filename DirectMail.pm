package Koha::Plugin::Com::RBitTechnology::DirectMail;


use Modern::Perl;
use base qw(Koha::Plugins::Base);
use Encode qw( decode );
use Text::CSV::Encoded;
use File::Temp;
use File::Basename qw( dirname );
use List::MoreUtils qw(uniq);
use utf8;
use C4::Context;
use C4::Letters;
use Koha::Patrons;
use Koha::Patron::Attribute::Types;
use Koha::Patron::Categories;
use JSON qw(to_json);
use Try::Tiny;

use Data::Dumper;

our $VERSION = "3.0.2";

our $extAttrIsOn = C4::Context->preference('ExtendedPatronAttributes') ne '0';
our $independentBranches = C4::Context->preference('IndependentBranches') ne '0';
our $letterCodeDirectmail = 'PLUGIN_DIRECTMAIL';

our $metadata = {
    name            => 'Přímé oslovování čtenářů',
    author          => 'Radek Šiman',
    description     => 'Využitím tohoto nástroje lze vytvářet cílové skupiny čtenářů podle zadaných kritérií a těmto skupinám rozesílat hromadně zprávy formou e-mailu. '
                         . ($extAttrIsOn ? '' : 'Modul nebude fungovat, není nastaven parametr <a href="/cgi-bin/koha/admin/preferences.pl?op=search&amp;searchfield=ExtendedPatronAttributes"><strong>ExtendedPatronAttributes</strong></a>.'),
    date_authored   => '2017-11-23',
    date_updated    => '2023-10-18',
    minimum_version => '21.11',
    maximum_version => undef,
    version         => $VERSION
};

sub new {
    my ( $class, $args ) = @_;

    ## We need to add our metadata here so our base class can access it
    $args->{'metadata'} = $metadata;
    $args->{'metadata'}->{'class'} = $class;

    ## Here, we call the 'new' method for our base class
    ## This runs some additional magic and checking
    ## and returns our actual $self
    my $self = $class->SUPER::new($args);

    return $self;
}

sub install() {
    my ( $self, $args ) = @_;

    my $table_predefs = $self->get_qualified_table_name('predefs');
    my $table_options = $self->get_qualified_table_name('predef_options');

    $self->add_mq_icon();

    return  C4::Context->dbh->do( "
        CREATE TABLE IF NOT EXISTS $table_predefs (
            `predef_id` INT( 11 ) NOT NULL AUTO_INCREMENT,
            `name` VARCHAR(80) NOT NULL,
            `description` TEXT DEFAULT NULL,
            `branchcode` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
            `date_created` DATETIME NOT NULL,
            `last_modified` DATETIME NOT NULL,
            PRIMARY KEY(`predef_id`),
            INDEX `fk_options_branches_idx` (`branchcode` ASC),
            CONSTRAINT `fk_directmail_predef_branches` FOREIGN KEY (`branchcode`) REFERENCES `branches` (`branchcode`) ON DELETE CASCADE ON UPDATE CASCADE
        ) ENGINE = INNODB DEFAULT CHARACTER SET = utf8 COLLATE = utf8_czech_ci;
        " ) && C4::Context->dbh->do( "
        CREATE TABLE IF NOT EXISTS $table_options (
            `predef_option_id` INT( 11 ) NOT NULL AUTO_INCREMENT,
            `predef_id` INT( 11 ) NOT NULL,
            `variable` VARCHAR(50) NOT NULL,
            `value` TEXT NOT NULL,
            PRIMARY KEY(`predef_option_id`),
            INDEX `fk_predef_options_idx` (`predef_id` ASC),
            CONSTRAINT `fk_directmail_predef_options` FOREIGN KEY (`predef_id`) REFERENCES `$table_predefs` (`predef_id`) ON DELETE CASCADE ON UPDATE CASCADE
        ) ENGINE = INNODB DEFAULT CHARACTER SET = utf8 COLLATE = utf8_czech_ci;
    " );

}

sub uninstall() {
    my ( $self, $args ) = @_;

    my $table_predefs = $self->get_qualified_table_name('predefs');
    my $table_options = $self->get_qualified_table_name('predef_options');

    return C4::Context->dbh->do("DROP TABLE $table_options") && C4::Context->dbh->do("DROP TABLE $table_predefs");
}

sub upgrade {
    my ( $self, $args ) = @_;

    my $upgraded = 0;

    my $table_predefs = $self->get_qualified_table_name('predefs');
    my $sth = C4::Context->dbh->prepare( "SELECT branchcode FROM $table_predefs WHERE 0 = 1" );
    $upgraded = $sth->execute() or ( C4::Context->dbh->do( "
        ALTER TABLE $table_predefs
            ADD COLUMN `branchcode` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL AFTER `description`,
            ADD INDEX `fk_options_branches_idx` (`branchcode` ASC),
            ADD CONSTRAINT `fk_directmail_predef_branches` FOREIGN KEY (`branchcode`) REFERENCES `branches` (`branchcode`) ON DELETE CASCADE ON UPDATE CASCADE
    " ) );

    $self->add_mq_icon();

    return $upgraded;
}

sub add_mq_icon {
    my ( $self, $args ) = @_;

    try {
	    my $table_mq_icons= 'koha_plugin_com_rbittechnology_messagequeue_icons';
	    my $sth = C4::Context->dbh->prepare( "SELECT icon_id FROM $table_mq_icons WHERE letter_code = ? LIMIT 1" );
	    if ( $sth->execute($letterCodeDirectmail) && !$sth->rows ) {  # table exists, but has no row for PLUGIN_DIRECTMAIL
	        C4::Context->dbh->do( "INSERT INTO $table_mq_icons (letter_code, fa_awesome, description) VALUES ('$letterCodeDirectmail', 'fa-hand-o-left', 'Přímé oslovování čtenářů')" );
	    }
    }
    catch {
        my $no_messagequeue_plugin = 1;     # nothing to do, plugin does not exist	
    }
}

sub configure {
    my ( $self, $args ) = @_;
    my $cgi = $self->{'cgi'};

    unless ( $cgi->param('save') ) {
        my $template = $self->get_template({ file => 'configure.tt' });

        ## Grab the values we already have for our settings, if any exist

        patron_attribute_type_list($template);

        $template->param(
            extended_attr => $extAttrIsOn,
            extattr_agree => $self->retrieve_data('extattr_agree'),
        );

        print $cgi->header(-type => 'text/html',
                           -charset => 'utf-8');
        print $template->output();
    }
    else {
        $self->store_data(
            {
                extattr_agree => $cgi->param('extattr_agree'),
                last_configured_by => C4::Context->userenv->{'number'},
            }
        );

        $self->go_home();
    }
}

sub patron_attribute_type_list {
    my $template = shift;

    my @attr_types = Koha::Patron::Attribute::Types->search->as_list; #C4::Members::AttributeTypes::GetAttributeTypes( 1, 1 );

    my @classes = uniq( map { $_->{class} } @attr_types );
    @classes = sort @classes;

    my @attributes_loop;
    for my $class (@classes) {
        #my ( @items, $branches );
        my @items,;
        for my $attr (@attr_types) {
            next if $attr->{class} ne $class;
#            my $attr_type = C4::Members::AttributeTypes->fetch($attr->{code});
#            $attr->{branches} = $attr_type->branches;
            push @items, $attr;
        }
        my $av = Koha::AuthorisedValues->search({ category => 'PA_CLASS', authorised_value => $class });
        my $lib = $av->count ? $av->next->lib : $class;
        push @attributes_loop, {
            class => $class,
            items => \@items,
            lib   => $lib
        };
    }
    $template->param(available_attribute_types => \@attributes_loop);
}


sub tool {
    my ( $self, $args ) = @_;

    my $cgi = $self->{'cgi'};

    unless ( $cgi->param('phase') ) {
        $self->tool_list_predefs();
    }
    elsif ( $cgi->param('phase') eq 'edit' ) {
        $self->tool_edit_predef();
    }
    elsif ( $cgi->param('phase') eq 'run' ) {
        $self->tool_get_results();
    }
    elsif ( $cgi->param('phase') eq 'delete' ) {
        $self->tool_delete_predef();
    }
    elsif ( $cgi->param('phase') eq 'duplicate' ) {
        $self->tool_duplicate_predef();
    }
    elsif ( $cgi->param('phase') eq 'mail' ) {
        $self->tool_send_mail();
    }
    elsif ( $cgi->param('phase') eq 'sql' ) {
        $self->tool_show_sql();
    }
    elsif ( $cgi->param('phase') eq 'letter' ) {
        print $cgi->header(-type => 'text/json',
                          -charset => 'utf-8');

        print to_json( { letter => $self->get_letter(scalar $cgi->param('code')) } );
    }
}

sub tool_list_predefs {
    my ( $self, $args ) = @_;
    my $cgi = $self->{'cgi'};

    my $template = $self->get_template({ file => 'tool-list.tt' });

    print $cgi->header(-type => 'text/html',
                       -charset => 'utf-8');

    if ( defined $cgi->param('gdpr') && $cgi->param('gdpr') eq 'yes' ) {
       $self->set_gdpr_agreements();
    }

    my $dbh = C4::Context->dbh;
    my $table_predefs = $self->get_qualified_table_name('predefs');
    my $where = $independentBranches ? ' WHERE branchcode = ?' : '';

    my $query = "SELECT predef_id, name, description, date_created, last_modified FROM $table_predefs $where;";

    my $sth = $dbh->prepare($query);
    $independentBranches ? $sth->execute(C4::Context->userenv->{'branch'}) : $sth->execute();

    my @results;
    while ( my $row = $sth->fetchrow_hashref() ) {
        push( @results, $row );
    }

    $template->param(
        predefs => \@results,
        extattr => $self->retrieve_data('extattr_agree'),
        borrowers => $self->check_borrowers
    );

    print $template->output();
}

sub is_denied {
    my ( $self, $predefId ) = @_;
    my $cgi = $self->{'cgi'};

    return 0 if !$independentBranches || $predefId eq 'none';

    my $dbh = C4::Context->dbh;
    my $table_predefs = $self->get_qualified_table_name('predefs');
    my $query = "SELECT predef_id FROM $table_predefs WHERE predef_id = ? AND branchcode = ?;";
    my $sth = $dbh->prepare($query);
    $sth->execute( $predefId, C4::Context->userenv->{'branch'} );
    my $row = $sth->fetchrow_hashref();

    if ( !$row || !exists $row->{predef_id} ) {
        print $cgi->redirect( '/cgi-bin/koha/plugins/run.pl?class=Koha::Plugin::Com::RBitTechnology::DirectMail&method=tool' );
        exit;
    }

    return 0;
}

sub tool_edit_predef {
    my ( $self, $args ) = @_;
    my $cgi = $self->{'cgi'};

    my $template = $self->get_template({ file => 'tool-edit.tt' });

    my $options = {};
    my $predef = undef;
    my $dbh = C4::Context->dbh;
    my $query;
    my $sth;

    if ( defined $cgi->param('predef') ) {
        $self->is_denied( scalar $cgi->param('predef') );

        my $table_options = $self->get_qualified_table_name('predef_options');
        my $table_predefs = $self->get_qualified_table_name('predefs');

        $query = "SELECT predef_id, name, description FROM $table_predefs WHERE predef_id = ?;";
        $sth = $dbh->prepare($query);
        $sth->execute( scalar $cgi->param('predef') );
        $predef = $sth->fetchrow_hashref();

        $query = "SELECT variable, value FROM $table_options WHERE predef_id = ?;";
        $sth = $dbh->prepare($query);
        $sth->execute( scalar $cgi->param('predef') );

        my %multi = map { $_ => 1 } qw(
            itemtypes
            services
            branches
            categories
        );

        while ( my $row = $sth->fetchrow_hashref() ) {
            if ( exists( $multi{$row->{variable}} ) ) {
                my @arr = split /,/, $row->{value};
                $options->{$row->{variable}} = \@arr;
            }
            else {
                $options->{$row->{variable}} = $row->{value};
            }
        }
    }

    $query = "SHOW TABLES LIKE 'account_debit_types';";
    $sth = $dbh->prepare($query);
    $sth->execute();
    my @acct_table = $sth->fetchall_arrayref();
    my @debit_types;
    if (@acct_table) {
        $query = "SELECT code, description FROM account_debit_types WHERE can_be_invoiced = 1 ORDER BY description;";
        $sth = $dbh->prepare($query);
        $sth->execute();
        while ( my $row = $sth->fetchrow_hashref() ) {
            push( @debit_types, $row );
        }
    }

    # prepare data for the form
    my @itemtypes = Koha::ItemTypes->search({}, { order_by => ['description'], columns => [qw/itemtype description/] } );
    my @invoice_types = Koha::AuthorisedValues->search({ category => 'MANUAL_INV'},  { order_by => ['lib'], columns  => [ {code => 'authorised_value'}, {description => 'lib'} ] } );
    my @branches;
    if ($independentBranches) {
        @branches = Koha::Libraries->search( {branchcode => C4::Context->userenv->{'branch'}}, { order_by => ['branchname'], columns => [qw/branchcode branchname/] } );
    }
    else {
        @branches = Koha::Libraries->search( {}, { order_by => ['branchname'], columns => [qw/branchcode branchname/] } );
    }
    
    my @categories;
    if ($independentBranches) {
        @categories = Koha::Patron::Categories->search_with_library_limits();
    }
    else {
        @categories = Koha::Patron::Categories->search( {}, { order_by => ['description'] } );
    }
    
    print $cgi->header(-type => 'text/html',
                       -charset => 'utf-8');

    $template->param(
        itemtypes => \@itemtypes,
        branches => \@branches,
        categories => \@categories,
        invoice_types => @acct_table ? \@debit_types : \@invoice_types,
        options => $options,
        predef => $predef,
    );

    print $template->output();
}

sub execute_sql {
    my ( $self, $predefId, $returnSql ) = @_;

    # retrieve column list
    my $dbh = C4::Context->dbh;
    my $table_options = $self->get_qualified_table_name('predef_options');

    $predefId = $predefId eq 'none' ? 0 : $predefId;

    # detect enabled subconditions
    my $enabled = {};
    my $query = "SELECT SUBSTRING(variable, 5) as subcond FROM $table_options WHERE variable LIKE 'chk_%' AND value = '1' AND predef_id = ?;";
    my $sth = $dbh->prepare($query);
    $sth->execute($predefId);
    while ( my $row = $sth->fetchrow_hashref() ) {
        $enabled->{$row->{subcond}} = 1;
    }

    # prepare subconditions
    my $subcond = {};
    $query = "SELECT variable, value FROM $table_options WHERE  variable NOT LIKE 'chk_%' AND predef_id = ?;";
    $sth = $dbh->prepare($query);
    $sth->execute($predefId);
    while ( my $row = $sth->fetchrow_hashref() ) {
        $subcond->{$row->{variable}} = $row->{value};
    }

    # find items referencing given genre/form
    my @itemsByGenre;
    if ( $enabled->{genre_form} ) {
        # find biblio adepts - contains the authority in xpath_expr result, but it can be a false positive, because partial results are space-concatenated
        $query = "SELECT biblionumber , ExtractValue( metadata , \"count(//datafield[\@tag='655']/subfield[\@code='a'])\") cnt655 FROM biblio_metadata WHERE biblionumber IN( SELECT biblionumber FROM biblio_metadata WHERE ExtractValue( metadata , \"//datafield[\@tag='655']/subfield[\@code='a']\") LIKE ?);";
        $sth = $dbh->prepare($query);
        $sth->execute( '%' . $subcond->{genre_form} . '%' );
        while ( my $row = $sth->fetchrow_hashref() ) {

            # we know count of genre/forms in this biblio, so iterate over them to be sure we really found the authority - there is "=" in HAVING so it must match exactly
            for my $i (1..$row->{cnt655}) {
                $query = "SELECT itemnumber FROM items WHERE biblionumber IN (SELECT biblionumber FROM biblio_metadata WHERE biblionumber = ? AND ExtractValue( metadata , \"//datafield[\@tag='655']/subfield[\@code='a'][$i]\") = ?);";
                my $sthBib = $dbh->prepare($query);
                $sthBib->execute( $row->{biblionumber}, $subcond->{genre_form} );
                if ( my $item = $sthBib->fetchrow_hashref() ) {
                    push( @itemsByGenre, $item->{itemnumber} );
                    last;
                }
            }
        }
    }

    # find items referencing given genre/form
    my @itemsByAuthor;
    if ( $enabled->{author} ) {
        # find biblio adepts - contains the authority in xpath_expr result, but it can be a false positive, because partial results are space-concatenated
        $query = "SELECT biblionumber , ExtractValue( metadata , \"count(//datafield[\@tag='100']/subfield[\@code='a'])\") cnt100 FROM biblio_metadata WHERE biblionumber IN( SELECT biblionumber FROM biblio_metadata WHERE ExtractValue( metadata , \"//datafield[\@tag='100']/subfield[\@code='a']\") LIKE ?);";
        $sth = $dbh->prepare($query);
        $sth->execute( '%' . $subcond->{author} . '%' );
        while ( my $row = $sth->fetchrow_hashref() ) {

            # we know count of authors in this biblio, so iterate over them to be sure we really found the authority - there is "=" in HAVING so it must match exactly
            for my $i (1..$row->{cnt100}) {
                $query = "SELECT itemnumber FROM items WHERE biblionumber IN (SELECT biblionumber FROM biblio_metadata WHERE biblionumber = ? AND ExtractValue( metadata , \"//datafield[\@tag='100']/subfield[\@code='a'][$i]\") = ?);";
                my $sthBib = $dbh->prepare($query);
                $sthBib->execute( $row->{biblionumber}, $subcond->{author} );
                if ( my $item = $sthBib->fetchrow_hashref() ) {
                    push( @itemsByAuthor, $item->{itemnumber} );
                    last;
                }
            }
        }
    }

    my @where;
    my @bindParams;
    my @bindParamsHaving;
    my @havingParts;
    my @queryCols = qw( borrowers.borrowernumber cardnumber );
    my $period = {
        day => 1,
        month => 30,
        year => 365
    };
    my $operator = {
        avg => '=',
        min => '>=',
        max => '<='
    };

    my $regValidDate = 'IF(borrowers.date_renewed IS NOT NULL, borrowers.date_renewed, IF(date(borrowers.dateexpiry) > NOW(), borrowers.dateenrolled, NOW()))';

    # SELECT parts must be processed before any WHERE params to keep the right order of bindParams
    if ( $enabled->{last_visit} ) {
        my $op = $subcond->{last_visit_period_op} eq 'le' ? '<=' : '>=';
        push( @queryCols, '(SELECT max(statistics.`datetime`) FROM statistics WHERE statistics.borrowernumber = borrowers.borrowernumber AND type IN( "payment" , "writeoff" , "renew" , "issue" , "return")) as last_visit' );
        push( @havingParts, "DATEDIFF(now(), last_visit) / ? $op ?" );
        push( @bindParamsHaving, $period->{$subcond->{last_visit_period_type}} );
        push( @bindParamsHaving, $subcond->{last_visit_period_length} );
    }
    if ( $enabled->{issues} ) {
        my $selectCount = 'SELECT count(*) FROM statistics WHERE statistics.borrowernumber = borrowers.borrowernumber AND type = "issue"';
        if ( $subcond->{issues_period_type} eq 'year' || $subcond->{issues_period_type} eq 'month' ) {
            push( @queryCols, "($selectCount AND DATEDIFF(now(), statistics.`datetime`) / ? <= ?) as issues" );
            push( @bindParams, $period->{$subcond->{issues_period_type}} );
            push( @bindParams, $subcond->{issues_period_length} );
        }
        elsif ( $subcond->{issues_period_type} eq 'this-year' ) {
            push( @queryCols, "($selectCount AND YEAR(statistics.`datetime`) = YEAR(now())) as issues" );
        }
        elsif ( $subcond->{issues_period_type} eq 'last-reg' ) {
            push( @queryCols, "($selectCount AND date(statistics.DATETIME) > $regValidDate) as issues" );
        }
        my $op = $subcond->{issues_op};
        push( @havingParts, "issues $op ?" );
        push( @bindParamsHaving, $subcond->{issues} );
    }
    if ( $enabled->{bbox} ) {
        my $selectCount = 'SELECT count(*) FROM old_issues WHERE old_issues.borrowernumber = borrowers.borrowernumber AND returndate < `timestamp`';
        if ( $subcond->{bbox_period_type} eq 'year' || $subcond->{bbox_period_type} eq 'month' ) {
            push( @queryCols, "($selectCount AND DATEDIFF(now(), returndate) / ? <= ?) as bbox" );
            push( @bindParams, $period->{$subcond->{bbox_period_type}} );
            push( @bindParams, $subcond->{bbox_period_length} );
        }
        elsif ( $subcond->{bbox_period_type} eq 'this-year' ) {
            push( @queryCols, "($selectCount AND YEAR(returndate) = YEAR(now())) as bbox" );
        }
        elsif ( $subcond->{issues_period_type} eq 'last-reg' ) {
            push( @queryCols, "($selectCount AND date(returndate) > $regValidDate) as bbox" );
        }
        my $op = $subcond->{bbox_op};
        push( @havingParts, "bbox $op ?" );
        push( @bindParamsHaving, $subcond->{bbox} );
    }
    if ( $enabled->{prolongs} ) {
        my $selectCount = 'SELECT count(*) FROM statistics WHERE statistics.borrowernumber = borrowers.borrowernumber AND type = "renew"';
        if ( $subcond->{prolongs_period_type} eq 'year' || $subcond->{prolongs_period_type} eq 'month' ) {
            push( @queryCols, "($selectCount AND DATEDIFF(now(), statistics.`datetime`) / ? <= ?) as prolongs" );
            push( @bindParams, $period->{$subcond->{prolongs_period_type}} );
            push( @bindParams, $subcond->{prolongs_period_length} );
        }
        elsif ( $subcond->{prolongs_period_type} eq 'this-year' ) {
            push( @queryCols, "($selectCount AND YEAR(statistics.`datetime`) = YEAR(now())) as prolongs" );
        }
        elsif ( $subcond->{prolongs_period_type} eq 'last-reg' ) {
            push( @queryCols, "($selectCount AND date(statistics.DATETIME) > $regValidDate) as prolongs" );
        }
        my $op = $subcond->{prolongs_op};
        push( @havingParts, "prolongs $op ?" );
        push( @bindParamsHaving, $subcond->{prolongs} );
    }
    if ( $enabled->{issue_length} ) {
        my $fn = $subcond->{issue_length_type};
        my $issueLength = "$fn(datediff(returndate, issuedate))";
        $issueLength = "ROUND($issueLength)" if ( $fn eq 'avg' );
        my $selectInner = "SELECT $issueLength FROM old_issues WHERE old_issues.borrowernumber = borrowers.borrowernumber AND DATE(returndate) != DATE(issuedate)";
        if ( $subcond->{issue_length_period_type} eq 'year' || $subcond->{issue_length_period_type} eq 'month' ) {
            push( @queryCols, "($selectInner AND DATEDIFF(now(), returndate) / ? <= ?) as issue_length" );
            push( @bindParams, $period->{$subcond->{issue_length_period_type}} );
            push( @bindParams, $subcond->{issue_length_period_length} );
        }
        elsif ( $subcond->{issue_length_period_type} eq 'this-year' ) {
            push( @queryCols, "($selectInner AND YEAR(returndate) = YEAR(now())) as issue_length" );
        }
        elsif ( $subcond->{issue_length_period_type} eq 'last-reg' ) {
            push( @queryCols, "($selectInner AND date(returndate) > $regValidDate) as issue_length" );
        }
        my $op = $operator->{$subcond->{issue_length_type}};
        push( @havingParts, "issue_length $op ?" );
        push( @bindParamsHaving, $subcond->{issue_length} );
    }
    if ( $enabled->{reserves} ) {
        my $selectCountOld = 'SELECT count(*) FROM old_reserves WHERE old_reserves.borrowernumber = borrowers.borrowernumber';
        my $selectCount = 'SELECT count(*) FROM reserves WHERE reserves.borrowernumber = borrowers.borrowernumber';
        if ( $subcond->{reserves_period_type} eq 'year' || $subcond->{reserves_period_type} eq 'month' ) {
            push( @queryCols, "($selectCount AND DATEDIFF(now(), reservedate) / ? <= ?) as reserves" );
            push( @bindParams, $period->{$subcond->{reserves_period_type}} );
            push( @bindParams, $subcond->{reserves_period_length} );

            push( @queryCols, "($selectCountOld AND DATEDIFF(now(), reservedate) / ? <= ?) as old_reserves" );
            push( @bindParams, $period->{$subcond->{reserves_period_type}} );
            push( @bindParams, $subcond->{reserves_period_length} );
        }
        elsif ( $subcond->{reserves_period_type} eq 'this-year' ) {
            push( @queryCols, "($selectCount AND YEAR(reservedate) = YEAR(now())) as reserves" );
            push( @queryCols, "($selectCountOld AND YEAR(reservedate) = YEAR(now())) as old_reserves" );
        }
        elsif ( $subcond->{reserves_period_type} eq 'last-reg' ) {
            push( @queryCols, "($selectCount AND date(reservedate) > $regValidDate) as reserves" );
            push( @queryCols, "($selectCountOld AND date(reservedate) > $regValidDate) as old_reserves" );
        }
        my $op = $subcond->{reserves_op};
        push( @havingParts, "reserves+old_reserves $op ?" );
        push( @bindParamsHaving, $subcond->{reserves} );
    }
    if ( $enabled->{fines} ) {
        my $selectCount = "SELECT count(*) FROM accountlines WHERE accountlines.borrowernumber = borrowers.borrowernumber AND debit_type_code IN ('F', 'OVERDUE')";
        if ( $subcond->{fines_period_type} eq 'year' || $subcond->{fines_period_type} eq 'month' ) {
            push( @queryCols, "($selectCount AND DATEDIFF(now(), accountlines.date) / ? <= ?) as fines" );
            push( @bindParams, $period->{$subcond->{fines_period_type}} );
            push( @bindParams, $subcond->{fines_period_length} );
        }
        elsif ( $subcond->{fines_period_type} eq 'this-year' ) {
            push( @queryCols, "($selectCount AND YEAR(accountlines.date) = YEAR(now())) as fines" );
        }
        elsif ( $subcond->{fines_period_type} eq 'last-reg' ) {
            push( @queryCols, "($selectCount AND date(accountlines.date) > $regValidDate) as fines" );
        }
        my $op = $subcond->{fines_op};
        push( @havingParts, "fines $op ?" );
        push( @bindParamsHaving, $subcond->{fines} );
    }
    if ( $enabled->{services} ) {
        my @paymentTypes = split(',', $subcond->{services});
        my @qMarks;
        foreach my $pay ( @paymentTypes ) {
            push( @bindParams, $pay );
            push( @qMarks, '?' );
        }
        my $selectCount = "SELECT count(*) FROM accountlines WHERE accountlines.borrowernumber = borrowers.borrowernumber AND debit_type_code IN (" . join(',', @qMarks) . ")";
        if ( $subcond->{services_count_period_type} eq 'year' || $subcond->{services_count_period_type} eq 'month' ) {
            push( @queryCols, "($selectCount AND DATEDIFF(now(), accountlines.date) / ? <= ?) as services" );
            push( @bindParams, $period->{$subcond->{services_count_period_type}} );
            push( @bindParams, $subcond->{services_count_period_length} );
        }
        elsif ( $subcond->{services_count_period_type} eq 'this-year' ) {
            push( @queryCols, "($selectCount AND YEAR(accountlines.date) = YEAR(now())) as services" );
        }
        elsif ( $subcond->{services_count_period_type} eq 'last-reg' ) {
            push( @queryCols, "($selectCount AND date(accountlines.date) > $regValidDate) as services" );
        }
        my $op = $subcond->{services_op};
        push( @havingParts, "services $op ?" );
        push( @bindParamsHaving, $subcond->{services_count} );
    }
    if ( $enabled->{itemtypes} ) {
        my @itypes = split(',', $subcond->{itemtypes});
        my @qMarks;
        foreach my $itype ( @itypes ) {
            push( @bindParams, $itype );
            push( @bindParams, $itype );    # must be twice because of issues, old_issues
            push( @qMarks, '?' );
        }
        push( @queryCols, "(SELECT count(*) FROM( SELECT issue_id FROM     issues LEFT JOIN items USING(itemnumber) WHERE itype IN(" . join(',', @qMarks) . ") LIMIT 1) AS T1) as itypes" );
        push( @queryCols, "(SELECT count(*) FROM( SELECT issue_id FROM old_issues LEFT JOIN items USING(itemnumber) WHERE itype IN(" . join(',', @qMarks) . ") LIMIT 1) AS T2) as old_itypes" );
        push( @havingParts, "itypes+old_itypes > 0" );
    }
    if ( $enabled->{genre_form} ) {
        push( @queryCols, "(SELECT issue_id FROM old_issues WHERE old_issues.borrowernumber = borrowers.borrowernumber AND itemnumber IN(". join(",", @itemsByGenre) . ") LIMIT 1) as genre" );
        push( @queryCols, "(SELECT issue_id FROM     issues WHERE     issues.borrowernumber = borrowers.borrowernumber AND itemnumber IN(". join(",", @itemsByGenre) . ") LIMIT 1) as old_genre" );
        push( @havingParts, "(genre IS NOT NULL OR old_genre IS NOT NULL)" );
    }
    if ( $enabled->{author} ) {
        push( @queryCols, "(SELECT issue_id FROM old_issues WHERE old_issues.borrowernumber = borrowers.borrowernumber AND itemnumber IN(". join(",", @itemsByAuthor) . ") LIMIT 1) as author" );
        push( @queryCols, "(SELECT issue_id FROM     issues WHERE     issues.borrowernumber = borrowers.borrowernumber AND itemnumber IN(". join(",", @itemsByAuthor) . ") LIMIT 1) as old_author" );
        push( @havingParts, "(author IS NOT NULL OR old_author IS NOT NULL)" );
    }

    # WHERE must be processed after SELECT parts to keep the right order of bindParams
    if ( $enabled->{sex} ) {
        unless ( $subcond->{sex} eq 'N' ) {
            push( @where, "borrowers.sex = ?" );
            push( @bindParams, $subcond->{sex} );
        }
        else {
            push( @where, "borrowers.sex NOT IN (\"M\", \"F\")" );
        }
    }
    if ( $enabled->{age} ) {
        push( @where, "YEAR(CURRENT_DATE) - YEAR(dateofbirth) - (RIGHT(CURRENT_DATE, 5) < RIGHT(dateofbirth, 5)) BETWEEN ? AND ?" );
        push( @bindParams, $subcond->{age_from} );
        push( @bindParams, $subcond->{age_to} );
    }
    if ( $enabled->{branches} ) {
        my @branches = split(',', $subcond->{branches});
        my @qMarks;
        foreach my $branch ( @branches ) {
            push( @bindParams, $branch );
            push( @qMarks, '?' );
        }
        push( @where, "borrowers.branchcode IN (" . join(',', @qMarks) . ")" );
    }
    if ( $independentBranches ) {
        push( @where, "borrowers.branchcode = ?" );
        push( @bindParams, C4::Context->userenv->{'branch'} );
    }
    if ( $enabled->{categories} ) {
        my @categories = split(',', $subcond->{categories});
        my @qMarks;
        foreach my $cat ( @categories ) {
            push( @bindParams, $cat );
            push( @qMarks, '?' );
        }
        push( @where, "borrowers.categorycode IN (" . join(',', @qMarks) . ")" );
    }
    if ( $enabled->{renew} ) {
        my $op = $subcond->{renew_period_op} eq 'le' ? '<=' : '>=';
        push( @where, "DATEDIFF(now(), IFNULL(borrowers.date_renewed, borrowers.dateenrolled)) / ? $op ?" );
        push( @bindParams, $period->{$subcond->{renew_period_type}} );
        push( @bindParams, $subcond->{renew_period_length} );
    }
    if ( $enabled->{expiry} ) {
        my $op = $subcond->{expiry_period_op} eq 'le' ? '<=' : '>=';
        push( @where, "borrowers.dateexpiry <= DATE(now())" );
        push( @where, "DATEDIFF(now(), IFNULL(borrowers.dateexpiry, now())) / ? $op ?" );
        push( @bindParams, $period->{$subcond->{expiry_period_type}} );
        push( @bindParams, $subcond->{expiry_period_length} );
    }

    # retrieve results to display
    my $dbColumns = join(',', @queryCols);
    my $having = (scalar @havingParts > 0) ? "HAVING " . join(' AND ', @havingParts) : '';
    my $subconditions = join(' AND ', @where);
    my $attrAcceptMails = $self->retrieve_data('extattr_agree') eq '?' ? '' : (' AND code ="' . $self->retrieve_data('extattr_agree') . '"');
    if ( $subconditions ) {
        $subconditions = " AND $subconditions";
    }

    $query = "SELECT email, smsalertnumber, IFNULL(attribute, -1) as gdpr, IF(TRIM(email) != \"\" AND email IS NOT NULL, 1, 0) as has_email, IF(TRIM(smsalertnumber) != \"\" AND smsalertnumber IS NOT NULL, 1, 0) as has_sms, $dbColumns "
        . " FROM borrowers "
        . " LEFT JOIN borrower_attributes ON borrower_attributes.borrowernumber = borrowers.borrowernumber $attrAcceptMails"
        . " WHERE ((TRIM(email) != \"\" AND email IS NOT NULL) OR (TRIM(smsalertnumber) != \"\" AND smsalertnumber IS NOT NULL)) $subconditions "
#            . " WHERE TRIM(email) != \"\" AND email IS NOT NULL AND attribute = 1 $subconditions "
        . "$having;";

    @bindParams = (@bindParams, @bindParamsHaving);

    if ($returnSql) {
        return ($query, @bindParams);
    }
    else {
        $sth = $dbh->prepare( $query );
        for my $i (0 .. $#bindParams) {
            $sth->bind_param($i + 1, $bindParams[$i]);
        }
        $sth->execute();

        return ($sth);
    }
}

sub check_borrowers {
    my ( $self, $args ) = @_;

    return undef if ( !defined($self->retrieve_data('extattr_agree')) || $self->retrieve_data('extattr_agree') eq '?' );

    my $dbh = C4::Context->dbh;
    my $attrAcceptMails = $self->retrieve_data('extattr_agree');
    my $branchCond = $independentBranches ? ' AND b.branchcode = ?' : '';

    my $query = "SELECT count(*) as total, sum(IF(attribute IS NULL, 1, 0)) as unknown, sum(IF(attribute = 1, 1, 0)) as yes, sum(IF(attribute = 0, 1, 0)) as no "
        . " FROM borrowers b "
        . " LEFT JOIN borrower_attributes a ON a.borrowernumber=b.borrowernumber AND a.CODE= \"$attrAcceptMails\""
        . " WHERE TRIM(email) != \"\" $branchCond;";
    my $sth = $dbh->prepare( $query );
    $independentBranches ? $sth->execute(C4::Context->userenv->{'branch'}) : $sth->execute();
    my $row = $sth->fetchrow_hashref();

    return $row;
}

sub set_gdpr_agreements {
    my ( $self, $args ) = @_;
    my $cgi = $self->{'cgi'};

    return if ( $self->retrieve_data('extattr_agree') eq '?' );

    my $attrAcceptMails = $self->retrieve_data('extattr_agree');
    my $dbh = C4::Context->dbh;
    my $branchCond = $independentBranches ? ' AND b.branchcode = ?' : '';

    my $query = "INSERT INTO borrower_attributes (borrowernumber, code, attribute)"
        . "SELECT b.borrowernumber, \"$attrAcceptMails\", 1 "
        . " FROM borrowers b "
        . " LEFT JOIN borrower_attributes a ON a.borrowernumber=b.borrowernumber AND a.CODE= \"$attrAcceptMails\""
        . " WHERE TRIM(email) != \"\" AND attribute IS NULL $branchCond;";
    my $sth = $dbh->prepare( $query );
    $independentBranches ? $sth->execute(C4::Context->userenv->{'branch'}) : $sth->execute();
}

sub tool_show_sql {
    my ( $self, $args ) = @_;
    my $cgi = $self->{'cgi'};
    my $predefId = scalar $cgi->param('predef');

    $self->is_denied( $predefId );

    my ($query, @bindParams) = $self->execute_sql($predefId, 1);

    my $template = $self->get_template({ file => 'tool-sql.tt' });

    print $cgi->header(-type => 'text/html',
                       -charset => 'utf-8');

    $query =~ s/(SELECT|FROM|WHERE|LEFT JOIN|IFNULL|TRIM|IS NOT NULL| AND| ON | as | AS | OR)/<strong>$1<\/strong>/g;
    $query =~ s/(FROM|LEFT JOIN|WHERE)/\n$1/g;
    $query =~ s/(\?)/<span style="background-color: #FFFF64; padding: 0 0.5em;"><strong>?<\/strong><\/span>/g;

    $template->param(
        query => $query,
        bind_params => \@bindParams
    );

    print $template->output();
}

sub tool_get_results {
    my ( $self, $args ) = @_;
    my $cgi = $self->{'cgi'};
    my $predefId;

    if ( defined $cgi->param('save') || defined $cgi->param('save_run') ) {
        $predefId = $self->tool_save_predef();
    }

    if ( defined $cgi->param('save') ) {
        $self->tool_list_predefs();
    }
    else {
        my $template = $self->get_template({ file => 'tool-results.tt' });

        if ( !defined $predefId && defined $cgi->param('predef') ) {
            $predefId = $cgi->param('predef');
        }

        $self->is_denied( $predefId );

        my ($sth) = $self->execute_sql($predefId, 0);

        my (
            @results_email_any, @results_email_yes,
            @results_sms_any, @results_sms_yes,
            @results_nomailsms_any, @results_nomailsms_yes
        );

        # hashes used to allow uniq emails and smsalertnumbers
        my (
            %results_email_any, %results_email_yes,
            %results_sms_any, %results_sms_yes,
            %results_nomailsms_any, %results_nomailsms_yes
        );
        while ( my $row = $sth->fetchrow_hashref() ) {
            $results_email_any{$row->{email}} = $row->{borrowernumber} if ( $row->{has_email} == 1 && !exists $results_email_any{$row->{email}} );
            $results_email_yes{$row->{email}} = $row->{borrowernumber} if ( $row->{has_email} == 1 && $row->{gdpr} == 1 && !exists $results_email_yes{$row->{email}} );
            $results_sms_any{$row->{smsalertnumber}} = $row->{borrowernumber} if ( $row->{has_sms} == 1 && !exists $results_sms_any{$row->{smsalertnumber}} );
            $results_sms_yes{$row->{smsalertnumber}} = $row->{borrowernumber} if ( $row->{has_sms} == 1 && $row->{gdpr} == 1 && !exists $results_sms_yes{$row->{smsalertnumber}} );
            $results_nomailsms_any{$row->{smsalertnumber}} = $row->{borrowernumber} if ( $row->{has_sms} == 1 && $row->{has_email} == 0 && !exists $results_nomailsms_any{$row->{smsalertnumber}} );
            $results_nomailsms_yes{$row->{smsalertnumber}} = $row->{borrowernumber} if ( $row->{has_sms} == 1 && $row->{has_email} == 0 && $row->{gdpr} == 1 && !exists $results_nomailsms_yes{$row->{smsalertnumber}} );
        }
        @results_email_any = values %results_email_any;
        @results_email_yes = values %results_email_yes;
        @results_sms_any = values %results_sms_any;
        @results_sms_yes = values %results_sms_yes;
        @results_nomailsms_any = values %results_nomailsms_any;
        @results_nomailsms_yes = values %results_nomailsms_yes;

        undef %results_email_any;
        undef %results_email_yes;
        undef %results_sms_any;
        undef %results_sms_yes;
        undef %results_nomailsms_any;
        undef %results_nomailsms_yes;

        my $letters = C4::Letters::GetLettersAvailableForALibrary(
            {
                branchcode => C4::Context->userenv->{'branch'},
                module     => 'members'
            }
        );

        my %avoid = (
            'ACCTDETAILS' => 1,
            'MEMBERSHIP_EXPIRY' => 1,
            'DISCHARGE' => 1,
            'SHARE_INVITE' => 1,
            'SHARE_ACCEPT' => 1,
            'PASSWORD_RESET' => 1,
            'OPAC_REG_VERIFY' => 1,
            'PROBLEM_REPORT' => 1,
        );
        my @filtered_letters;
        for my $l (@$letters) {
            next if ( exists $avoid{$l->{code}} );
            push @filtered_letters, $l;
        }

        print $cgi->header(-type => 'text/html',
                           -charset => 'utf-8');

        $template->param(
            borrowers_email_any => join(',', @results_email_any),
            borrowers_email_yes => join(',', @results_email_yes),
            borrowers_sms_any => join(',', @results_sms_any),
            borrowers_sms_yes => join(',', @results_sms_yes),
            borrowers_nomailsms_any => join(',', @results_nomailsms_any),
            borrowers_nomailsms_yes => join(',', @results_nomailsms_yes),
            count_email_any => scalar @results_email_any,
            count_email_yes => scalar @results_email_yes,
            count_sms_any => scalar @results_sms_any,
            count_sms_yes => scalar @results_sms_yes,
            count_nomailsms_any => scalar @results_nomailsms_any,
            count_nomailsms_yes => scalar @results_nomailsms_yes,
            letters => \@filtered_letters,
            predef => $predefId,
            sms_allowed => C4::Context->preference('SMSSendDriver') ne ''
        );

        print $template->output();
    }

}

sub tool_save_predef {
    my ( $self, $args ) = @_;
    my $cgi = $self->{'cgi'};

    my $dbh = C4::Context->dbh;
    my $table_predefs = $self->get_qualified_table_name('predefs');
    my $table_options = $self->get_qualified_table_name('predef_options');

    my $predef_id;
    my $query;
    my $sth;

    if ( defined $cgi->param('predef') ) {
        $query = "UPDATE $table_predefs SET name = ?, description = ?, last_modified = now() WHERE predef_id = ?;";

        $predef_id = scalar $cgi->param('predef');
        $self->is_denied( $predef_id );

        $sth = $dbh->prepare($query);
        $sth->execute(
            defined $cgi->param('predef-name') ? scalar $cgi->param('predef-name') : '(nepojmenováno)',
            defined $cgi->param('predef-descr') ? scalar $cgi->param('predef-descr') : undef,
            $predef_id
        );

        $query = "DELETE FROM $table_options WHERE predef_id = ?;";
        $sth = $dbh->prepare($query);
        $sth->execute($predef_id);
    }
    else {
        $query = "INSERT INTO $table_predefs (name, description, branchcode, date_created, last_modified) VALUES(?, ?, ?, now(), now());";

        $sth = $dbh->prepare($query);
        $sth->execute(
            defined $cgi->param('predef-name') ? scalar $cgi->param('predef-name') : '(nepojmenováno)',
            defined $cgi->param('predef-descr') ? scalar $cgi->param('predef-descr') : undef,
            $independentBranches ? C4::Context->userenv->{'branch'} : undef
        );

        $predef_id = $dbh->last_insert_id(undef, undef, $table_predefs, 'predef_id');
    }


    my @fields = qw(
        chk_sex sex
        chk_age age_from age_to
        chk_branches branches
        chk_categories categories
        chk_last_visit last_visit_period_length last_visit_period_type last_visit_period_op
        chk_renew renew_period_length renew_period_type renew_period_op
        chk_expiry expiry_period_length expiry_period_type expiry_period_op
        chk_issues issues_op issues issues_period_length issues_period_type
        chk_reserves reserves_op reserves reserves_period_length reserves_period_type
        chk_bbox bbox_op bbox bbox_period_length bbox_period_type
        chk_itemtypes itemtypes
        chk_genre_form genre_form
        chk_author author
        chk_issue_length issue_length issue_length_type issue_length_period_length issue_length_period_type
        chk_prolongs prolongs prolongs_op prolongs_period_length prolongs_period_type
        chk_fines fines fines_op fines_period_length fines_period_type
        chk_services services services_count services_op services_count_period_length services_count_period_type
    );
    my %multi = map { $_ => 1 } qw(
        itemtypes
        services
        branches
        categories
    );

    my @data;
    for my $f (@fields) {
        if ( defined $cgi->param($f) ) {
            my $value;
            if ( exists( $multi{$f} ) ) {
                my @options = $cgi->multi_param($f);
                $value = join(',', @options);
            }
            else {
                $value = scalar $cgi->param($f);
            }
            push( @data, ($predef_id, $f, $value) );
        }
    }

    $query = "INSERT INTO $table_options (predef_id, variable, value) VALUES ";
    $query .= "(?, ?, ?)," x (scalar @data / 3);
    $query =~ s/,$/;/g;

    $sth = $dbh->prepare($query);
    $sth->execute( @data );

    return $predef_id;
}

sub tool_delete_predef {
    my ( $self, $args ) = @_;
    my $cgi = $self->{'cgi'};

    if ( defined $cgi->param('predef') ) {
        $self->is_denied( scalar $cgi->param('predef') );

        my $dbh = C4::Context->dbh;
        my $table_predefs = $self->get_qualified_table_name('predefs');

        my $query = "DELETE FROM $table_predefs WHERE predef_id = ?;";

        my $sth = $dbh->prepare($query);
        $sth->execute( scalar $cgi->param('predef') );
    }

    $self->tool_list_predefs();
}

sub tool_duplicate_predef {
    my ( $self, $args ) = @_;
    my $cgi = $self->{'cgi'};

    if ( defined $cgi->param('predef') ) {
        $self->is_denied( scalar $cgi->param('predef') );

        my $dbh = C4::Context->dbh;
        my $table_predefs = $self->get_qualified_table_name('predefs');
        my $table_options = $self->get_qualified_table_name('predef_options');

        my $query = "INSERT INTO $table_predefs (name, description, date_created, last_modified) SELECT CONCAT('(kopie ', ? ') ', name), description, now(), now() FROM $table_predefs WHERE predef_id = ?;";
        my $sth = $dbh->prepare($query);
        $sth->execute( $cgi->param('predef'), $cgi->param('predef') );
        my $new_predef_id = $dbh->last_insert_id(undef, undef, $table_predefs, 'predef_id');

        $query = "INSERT INTO $table_options (predef_id, variable, value) SELECT ?, variable, value FROM $table_options WHERE predef_id = ?;";
        $sth = $dbh->prepare($query);
        $sth->execute( $new_predef_id, $cgi->param('predef') );
    }

    $self->tool_list_predefs();
}

sub enqueue_letter {
    my ( $self, $borrower, $transport ) = @_;
    my $cgi = $self->{'cgi'};

    if ( $independentBranches && $borrower->branchcode ne C4::Context->userenv->{'branch'} ) {
        return 0;
    }

    my $library = Koha::Libraries->find($borrower->branchcode)->unblessed;
    my $letter;

    if ( $transport eq 'email' ) {
        $letter->{'content'} = $cgi->param('msgtext');
    }
    elsif ( $transport eq 'sms' ) {
        $letter->{'content'} = $cgi->param('msgsms');
    }

    $letter = C4::Letters::GetPreparedLetter (
        message_transport_type => $transport,
        module => 'members',
        letter => $letter,
        branchcode => $borrower->branchcode,
        tables => {
            'branches'    => $library,
            'borrowers'   => $borrower->unblessed,
        },
    );

    if ( !$letter ) {
        $letter->{'code'} = $letterCodeDirectmail;
        $letter->{'message_transport_type'} = $transport;
    }
    elsif ( defined $cgi->param('letter') ) {
        $letter->{'code'} = scalar $cgi->param('letter');
    }

    if ( $transport eq 'email' ) {
        $letter->{'title'} = $cgi->param('msgsubj');
        $letter->{'content-type'} = ( defined $cgi->param('is_html') && scalar $cgi->param('is_html') )
                                      ? 'text/html; charset="utf-8"'
                                      : 'text/plain; charset="utf-8"';
    }
    elsif ( $transport eq 'sms' ) {
        $letter->{'title'} = 'Hromadná SMS';
    }


    C4::Letters::EnqueueLetter(
        {   letter                 => $letter,
            borrowernumber         => $borrower->borrowernumber,
            message_transport_type => $transport,
            from_address           => $library->{'branchemail'} || C4::Context->preference('KohaAdminEmailAddress'),
            to_address             => $transport eq 'sms' ? undef : $borrower->email
        }
    );

    return 1;
}

sub enqueue_email {
    my ( $self, $args ) = @_;
    my $cgi = $self->{'cgi'};

    my @idList = split(/,/, $cgi->param('recipients_email'));
    my @borrowers = Koha::Patrons->search({borrowernumber => { -in => \@idList }});

    my $sent = 0;

    foreach my $borrower (@borrowers) {
        if ( !$borrower->email ) {
            next;
        }

        $sent++ if $self->enqueue_letter($borrower, 'email');
    }

    return $sent;
}

sub enqueue_sms {
    my ( $self, $args ) = @_;
    my $cgi = $self->{'cgi'};

    my @idList = split(/,/, $cgi->param('recipients_sms'));
    my @borrowers = Koha::Patrons->search({borrowernumber => { -in => \@idList }});

    my $sent = 0;

    foreach my $borrower (@borrowers) {
        if ( !$borrower->smsalertnumber ) {
            next;
        }

        $sent++ if $self->enqueue_letter($borrower, 'sms');
    }

    return $sent;
}

sub tool_send_mail {
    my ( $self, $args ) = @_;
    my $cgi = $self->{'cgi'};

    my $template = $self->get_template({ file => 'tool-send-mail.tt' });
    print $cgi->header(-type => 'text/html',
                       -charset => 'utf-8');

    my $sent_mails = 0;
    my $sent_sms = 0;

    if ( defined $cgi->param('recipients_email') && defined $cgi->param('email') && scalar $cgi->param('email') ) {
        $sent_mails = $self->enqueue_email(),
    }

    if ( defined $cgi->param('recipients_sms') && defined $cgi->param('sms') && scalar $cgi->param('sms') ) {
        $sent_sms = $self->enqueue_sms(),
    }
    if ( $sent_mails || $sent_sms ) {
        $template->param(
            sent_mails => $sent_mails,
            sent_sms => $sent_sms,
            status => 'ok'
        );
    }
    else {
        $template->param(
            status => 'error'
        );
    }

    print $template->output();
}

sub get_letter {
  my ( $self, $code ) = @_;

  if ( defined $code ) {
      my $dbh = C4::Context->dbh;

      my $query = "SELECT title, content, is_html FROM letter WHERE code = ? AND module = 'members' AND message_transport_type = ?;";
      my $sth = $dbh->prepare($query);

      $sth->execute( $code, 'email' );
      my $rowEmail = $sth->fetchrow_hashref();

      $sth->execute( $code, 'sms' );
      my $rowSms = $sth->fetchrow_hashref();

      my $result = {
        email => $rowEmail ? $rowEmail : 0,
        sms => $rowSms ? $rowSms : 0,
      };

      return $result;
  }
}

1;
